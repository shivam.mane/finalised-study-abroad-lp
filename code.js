$("#submit_btn").click(function (e) {
  e.preventDefault();

  var name = $.trim($("#name").val());
  var email = $.trim($("#email").val());
  var phone = $.trim($("#phone").val());
  var state = $.trim($("#state").val());
  var city = $.trim($("#city").val());
  var experience = $.trim($("#experience").val());
  // var grecaptcha =$.trim($('#grecaptcha').val());
  var utm_source = $.trim($("#utm_source").val());
  var utm_medium = $.trim($("#utm_medium").val());
  var utm_campaign = $.trim($("#utm_campaign").val());

  if (!name) {
    $("#allerror").html("Enter Your Name");
    return false;
  }
  if (/[^a-zA-Z \-]/.test(name)) {
    $("#allerror").html("Enter only alphabets in name");
    return false;
  }
  if (!email) {
    $("#allerror").html("Enter your Email Address");
    return false;
  }
  var emailReg =
    /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
  if (emailReg.test(email) == false) {
    $("#allerror").html("Enter Valid Email Address.");
    return false;
  }
  var mob = /^[0-9]{10}$/;
  if (!phone) {
    $("#allerror").html("Enter Your Contact Number");
    return false;
  }
  if (mob.test(phone) == false) {
    $("#allerror").html("Enter only 10 digits in Contact Number");
    return false;
  }

  if (!state) {
    $("#allerror").html("Select Your State");
    return false;
  }

  if (!city) {
    $("#allerror").html("Select Your City");
    return false;
  }

  if (!experience) {
    $("#allerror").html("Select Your experience");
    return false;
  }
  if (grecaptcha.getResponse(0) == "") {
    e.preventDefault();
    $("#allerror").html("Please Verify Your Captcha");
    return false;
  } else {
    $("#allerror").html("");
  }
  var g_res = grecaptcha.getResponse(0);

  // alert(grecaptcha.getResponse());
  // exit();
  // $("#submit_btn").prop('disabled', true);
  alert("hellow ");
  $.ajax({
    url: "submitenquiry.php",
    type: "POST",
    dataType: "json",
    data: {
      name: name,
      email: email,
      phone: phone,
      state: state,
      city: city,
      experience: experience,
      utm_source: utm_source,
      utm_medium: utm_medium,
      utm_campaign: utm_campaign,
      g_res: grecaptcha.getResponse(0),
    },
    success: function (response) {
      // console.log(response)
      if (response.status == "1") {
        window.location.href =
          "thankyou.php?utm_source=" +
          utm_source +
          "&utm_medium=" +
          utm_medium +
          "&utm_campaign=" +
          utm_campaign;
        // alert();
      } else {
        alert("please try again.");
      }
    },
  });
});

/*mobile form */

$("#submit_btn_mob").click(function (e) {
  e.preventDefault();

  var name = $.trim($("#name1").val());
  var email = $.trim($("#email1").val());
  var phone = $.trim($("#phone1").val());
  var state = $.trim($("#state1").val());
  var city = $.trim($("#city1").val());
  var experience = $.trim($("#experience").val());
  var utm_source = $.trim($("#utm_source1").val());
  var utm_medium = $.trim($("#utm_medium1").val());
  var utm_campaign = $.trim($("#utm_campaign1").val());

  if (!name) {
    $("#allerror").html("Enter Your Name");
    return false;
  }
  if (/[^a-zA-Z \-]/.test(name)) {
    $("#allerror").html("Enter only alphabets in name");
    return false;
  }
  if (!email) {
    $("#allerror").html("Enter your Email Address");
    return false;
  }
  var emailReg =
    /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
  if (emailReg.test(email) == false) {
    $("#allerror").html("Enter Valid Email Address.");
    return false;
  }
  var mob = /^[0-9]{10}$/;
  if (!phone) {
    $("#allerror").html("Enter Your Contact Number");
    return false;
  }
  if (mob.test(phone) == false) {
    $("#allerror").html("Enter only 10 digits in Contact Number");
    return false;
  }

  if (!state) {
    $("#allerror").html("Select Your State");
    return false;
  }

  if (!city) {
    $("#allerror").html("Select Your City");
    return false;
  }

  if (!experience) {
    $("#allerror").html("Select Your experience");
    return false;
  }

  if (grecaptcha.getResponse(1) == "") {
    e.preventDefault();
    $("#allerror").html("Please Verify Your Captcha");
    return false;
  }
  var g_res = grecaptcha.getResponse(1);

  // alert(grecaptcha.getResponse());
  // exit();

  $("#submit_btn_mob").prop("disabled", true);
  $.ajax({
    url: "submitenquiry.php",
    type: "POST",
    dataType: "json",
    data: {
      name: name,
      email: email,
      phone: phone,
      state: state,
      city: city,
      experience: experience,
      utm_source: utm_source,
      utm_medium: utm_medium,
      utm_campaign: utm_campaign,
      g_res: grecaptcha.getResponse(1),
    },
    success: function (response) {
      // console.log(response)
      if (response.status == "1") {
        window.location.href =
          "thankyou.php?utm_source=" +
          utm_source +
          "&utm_medium=" +
          utm_medium +
          "&utm_campaign=" +
          utm_campaign;
        // alert();
      } else {
        alert("please try again.");
      }
    },
  });
});
