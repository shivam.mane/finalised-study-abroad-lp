<!DOCTYPE html>
<html lang="en">
<head>
    <title>Finalised-Study-Abroad-LP</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/jquery.fancybox.min.css">
    <link rel="stylesheet" href="styles/aos.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <link rel="stylesheet" href="styles/animate.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="styles/responsive.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer>
    </script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS shivam's log in -->
    <!-- <script type="text/javascript">
        var onloadCallback = function() {
            alert("grecaptcha is ready!");
        };
    </script> -->
</head>

<body id="about-us" data-spy="scroll" data-target=".navbar" data-offset="120">
    <header id="header" class="fixed-top">
        <nav class="navbar navbar-light navbar-expand-lg w-100">
            <div class="container-fluid navbar-margin">
                <a class="navbar-brand float-md-left logo-wd" href="index.php"><img alt="logo" src="images/images/Finalised-Study-Abroad-LP/logo.png" class="img-fluid" />
                </a>
                <!-- Toggler/collapsibe Button -->
                <button class="navbar-toggler float-right mt-0" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!-- Navbar links -->
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#about">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#courses">Courses</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#facilities">Facilities</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#campus">Campus</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#event">Events</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#recruiters">Recruiters</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
</body>