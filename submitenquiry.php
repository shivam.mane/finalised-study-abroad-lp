<?PHP
   // ini_set('display_errors', 1);
   // ini_set('display_startup_errors', 1);
   // error_reporting(E_ALL);
   

     include("dbconfig.php");
          
     $current_date=date('d-m-y');
     $conn=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);

  /*captcha starts*/

  function google_recaptch_validations($privatekey,$g_response){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, [
        'secret' => $privatekey,
        'response' => $g_response
    ]);

    $resp = json_decode(curl_exec($ch));
    curl_close($ch);

    if ($resp->success) {
        return true;
    } else {
        return false;
    }
  }

  /*captcha ends*/


   if (isset($_POST['name'])) {
        $fname = htmlspecialchars($_POST['name']);
    }

    if (isset($_POST['email'])) {
        $email = htmlspecialchars($_POST['email']);
    }
    if (isset($_POST['phone'])) {
      $mobile = htmlspecialchars($_POST['phone']);
  }

    if (isset($_POST['state'])) {
        $campus = htmlspecialchars($_POST['state']);
    }

    if (isset($_POST['city'])) {
        $program = htmlspecialchars($_POST['city']);
    }

     if (isset($_POST['experience'])) {
        $course = htmlspecialchars($_POST['experience']);
    }

    if (isset($_POST['utm_source'])) {
        $utm_source = htmlspecialchars($_POST['utm_source']);
    }
    
    if (isset($_POST['utm_medium'])) {
        $utm_medium = htmlspecialchars($_POST['utm_medium']);
    }

    if (isset($_POST['utm_campaign'])) {
        $utm_campaign = htmlspecialchars($_POST['utm_campaign']);
    }


$privatekey="6LcPkjYeAAAAAB4_7bnK6Iku17IjBGBRnfQknWo2"; // google recaptcha local secret key 

$g_response=$_POST['g_res'];


if (mysqli_connect_errno())
      {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
      }
      
else{  
          if(google_recaptch_validations($privatekey,$g_response)){

            $stmt = $conn->prepare("INSERT INTO `pharma_demo_lp` (`name`,`email`,`phone`,`state`,`city`,`experience`,`utm_source`,`utm_medium`,`utm_campaign`,`create_date`)
            VALUES (?,?,?,?,?,?,?,?,?,?)");
            $stmt->bind_param('ssssssssss',$name,$email,$phone,$state,$city,$experience,$utm_source, $utm_medium, $utm_campaign, $current_date);
            echo $stmt;
          
             $result = $stmt->execute();     

              if ($result) {
                $res=array("status"=>"1"); 
                echo json_encode($res);
              }
              else{
                $res=array("status"=>"0"); 
                echo json_encode($res);
              }

      }

      else {
          $res=array("status"=>"0");
          echo json_encode($res);
      }

}
    
  ?>