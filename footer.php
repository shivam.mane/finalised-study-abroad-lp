<section class="copyright py-4">
   <div class="container">
      <div class="row text-lg-center">
         <div class="col-lg-12">
            <h6 class="text-white">© Copyright 2021. All Rights Reserved by Parul University.</h6>
         </div>
      </div>
   </div>
</section>

<!-- <button class="btn scroll" id="scroll1" style="display:none;"><i class="fa fa-angle-up"></i>
</button> -->

<div class="fixed-bottom d-lg-none">
   <div class="bottom_button">
      <a href="#home" class="anchor" style="" id="scroll1">
         <button class="btn enq_ftr ">
            Apply Now</button></a>
   </div>
</div>


<script src="scripts/jquery.min.js"></script>
<script src="scripts/popper.min.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/wow.min.js"></script>
<script defer src="owlCarousel/js/owl.carousel.min.js"></script>
<script src="scripts/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="scripts/scripts.js"></script>
<script src="scripts/aos.js"></script>
<script type="text/javascript" src="<?php echo $folder; ?>/code.js"></script>
<script src="form_submit.js"></script>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

</body>

</html>