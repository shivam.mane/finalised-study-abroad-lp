 <?php include('header.php');
    ?>

 <!-- desktop form starts -->

 <section class="banner-section" id="home">
     <div class="container-fluid px-0">
         <div class="row no-gutters">
             <div class="col-md-12">
                 <div class="position-relative">
                     <img src="images/images/Finalised-Study-Abroad-LP/Banner.png" alt="Parul University" class="w-100 img-fluid">
                 </div>
             </div>
         </div>

         <div class="row no-gutters banner-sec2">
             <div id="inner">
                 <div class="col-sm-7 nopd">

                 </div>
                 <div class="col-sm-5">
                     <div class="formsec" id="form-scroll">
                         <div class="dsu-form">
                             <div class="form-clickOuter"><span class="form-click opacityprimary open">Apply Now</span>
                             </div>
                             <span class="form-sec">

                                 <div class="frmBg">
                                     <span>Get more info!</span>
                                 </div>

                                 <div class="contact_fild px-2">
                                     <div class="banner_form wow bounceInDown">
                                         <div class="banner-form">
                                             <div class="form-title">
                                                 <!-- <h5 class="black_text text-uppercase pb-2">Enquiry Form</h5> -->
                                             </div>
                                             <form name="enquiry_form" id="enquiry_form" method="post">
                                                 <div class="form_div">
                                                     <div class="">

                                                         <div id="allerror" class="font-weight-bold text-orange">
                                                         </div>

                                                         <?php
                                                            if (isset($_GET['utm_source']) && $_GET['utm_source'] != "") {
                                                                $utm_source = $_GET['utm_source'];
                                                            }
                                                            if (isset($_GET['utm_medium']) && $_GET['utm_medium'] != "") {
                                                                $utm_medium = $_GET['utm_medium'];
                                                            }
                                                            if (isset($_GET['utm_campaign']) && $_GET['utm_campaign'] != "") {
                                                                $utm_campaign = $_GET['utm_campaign'];
                                                            }


                                                            ?>
                                                         <input type="hidden" id="utm_source" name="utm_source" value="<?php if (isset($utm_source)) {
                                                                                                                            echo $utm_source;
                                                                                                                        } ?>" />
                                                         <input type="hidden" id="utm_medium" name="utm_medium" value="<?php if (isset($utm_medium)) {
                                                                                                                            echo $utm_medium;
                                                                                                                        } ?>" />
                                                         <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php if (isset($utm_campaign)) {
                                                                                                                                echo $utm_campaign;
                                                                                                                            } ?>" />


                                                         <div class="input-group mb-2">
                                                             <input type="text" class="form-control" placeholder="Name" name="name" id="name" maxlength="50">
                                                         </div>

                                                         <div class="input-group mb-2">
                                                             <input type="text" class="form-control" placeholder="Email" name="email" id="email">
                                                         </div>
                                                         <div class="input-group mb-2">
                                                             <input type="text" class="form-control" placeholder="Phone No" name="phone" id="phone">
                                                         </div>
                                                         <div class="input-group mb-2">
                                                             <select class="form-control" id="state" name="state">
                                                                 <option value="">Select State</option>
                                                                 <option value="Maharashtra"> Maharashtra</option>
                                                                 <option value="Madhay Pradesh">Madhay Pradesh</option>
                                                             </select>
                                                         </div>
                                                         <div class="input-group mb-2">
                                                             <select class="form-control" id="city" name="city">
                                                                 <option value="">Select City</option>
                                                                 <option value="Maharashtra"> Pune</option>
                                                                 <option value="Madhay Pradesh">Nashik</option>
                                                             </select>
                                                         </div>

                                                         <div class="input-group mb-2">
                                                             <select class="form-control" id="experience" name="experience">
                                                                 <option value="">Work Experience</option>
                                                                 <option value="Maharashtra">
                                                                     < 1 year</option>
                                                                 <option value="Madhay Pradesh">2 year</option>
                                                             </select>
                                                         </div>

                                                         <!-- Local Captcha -->
                                                         <div class="input-group mb-3">
                                                             <div class="g-recaptcha" data-sitekey="6LfLj4MfAAAAAO7dLTGdQ9DxVRKjcDy3ZCkkdme1" id="grecaptcha" name="grecaptcha">
                                                             </div>
                                                         </div>



                                                         <div class="mb-3 mt-2 d-flex text-center justify-content-end">
                                                             <!-- <input class="submit_btn btn mr-3" id="submit_btn" type="button"
                                                        value="Submit"> -->
                                                             <button class="submit_btn btn mr-3" id="submit_btn" type="button">Submit</button>
                                                         </div>

                                                         <div class="mb-3 mt-2">
                                                         </div>
                                                     </div>
                                                 </div>
                                             </form>
                                         </div>
                                     </div>
                                 </div>


                             </span>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section>
 <!-- desktop form ends -->



 <!-- Mobile form starts -->
 <section class="mobile-view-form my-mobile">
     <div class="container">
         <div class="row justify-content-center">
             <div class="col-12">

                 <div class="formsec1" id="form-scroll1">
                     <div class="dsu-form1">

                         <span class="form-sec1">

                             <div class="banner_form wow bounceInDown">
                                 <div class="banner-form">
                                     <div class="form-title">
                                         <h5 class="black_text text-uppercase pb-2">Enquiry Form</h5>
                                     </div>
                                     <form name="enquiry_form" id="enquiry_form" method="post">
                                         <div class="form_div">
                                             <div class="">

                                                 <div id="allerror" class="font-weight-bold text-orange">
                                                 </div>

                                                 <?php
                                                    if (isset($_GET['utm_source']) && $_GET['utm_source'] != "") {
                                                        $utm_source = $_GET['utm_source'];
                                                    }
                                                    if (isset($_GET['utm_medium']) && $_GET['utm_medium'] != "") {
                                                        $utm_medium = $_GET['utm_medium'];
                                                    }
                                                    if (isset($_GET['utm_campaign']) && $_GET['utm_campaign'] != "") {
                                                        $utm_campaign = $_GET['utm_campaign'];
                                                    }


                                                    ?>
                                                 <input type="hidden" id="utm_source" name="utm_source" value="<?php if (isset($utm_source)) {
                                                                                                                    echo $utm_source;
                                                                                                                } ?>" />
                                                 <input type="hidden" id="utm_medium" name="utm_medium" value="<?php if (isset($utm_medium)) {
                                                                                                                    echo $utm_medium;
                                                                                                                } ?>" />
                                                 <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php if (isset($utm_campaign)) {
                                                                                                                        echo $utm_campaign;
                                                                                                                    } ?>" />


                                                 <div class="input-group mb-2">
                                                     <input type="text" class="form-control" placeholder="Name" name="name" id="name" maxlength="50">
                                                 </div>

                                                 <div class="input-group mb-2">
                                                     <input type="text" class="form-control" placeholder="Email" name="email" id="email">
                                                 </div>
                                                 <div class="input-group mb-2">
                                                     <input type="text" class="form-control" placeholder="Phone No" name="phone" id="phone">
                                                 </div>
                                                 <div class="input-group mb-2">
                                                     <select class="form-control" id="state" name="state">
                                                         <option value="">Select State</option>
                                                         <option value="Maharashtra"> Maharashtra</option>
                                                         <option value="Madhay Pradesh">Madhay Pradesh</option>
                                                     </select>
                                                 </div>
                                                 <div class="input-group mb-2">
                                                     <select class="form-control" id="city" name="city">
                                                         <option value="">Select City</option>
                                                         <option value="Maharashtra"> Pune</option>
                                                         <option value="Madhay Pradesh">Nashik</option>
                                                     </select>
                                                 </div>

                                                 <div class="input-group mb-2">
                                                     <select class="form-control" id="experience" name="experience">
                                                         <option value="">Work Experience</option>
                                                         <option value="Maharashtra">
                                                             < 1 year</option>
                                                         <option value="Madhay Pradesh">2 year</option>
                                                     </select>
                                                 </div>

                                                 <!-- Local Captcha -->
                                                 <div class="input-group mb-3">
                                                     <div class="g-recaptcha" data-sitekey="6LfLj4MfAAAAAO7dLTGdQ9DxVRKjcDy3ZCkkdme1" id="grecaptcha" name="grecaptcha">
                                                     </div>
                                                 </div>



                                                 <div class="mb-3 mt-2 d-flex text-center justify-content-end">
                                                     <!-- <input class="submit_btn btn mr-3" id="submit_btn" type="button"
                                                        value="Submit"> -->
                                                     <button class="submit_btn btn mr-3" id="submit_btn" type="button">Submit</button>
                                                 </div>

                                                 <div class="mb-3 mt-2">
                                                 </div>
                                             </div>
                                         </div>
                                     </form>
                                 </div>
                             </div>



                         </span>
                     </div>
                 </div>

             </div>
         </div>
     </div>
 </section>
 <!-- Mobile form ends -->



 <!-- >About Parul University section starts -->
 <section class="learning-msc my-5 pt-lg-5" id="about">
     <div class="container">
         <div class="row align-items-center">
             <div class="col-lg-6">
                 <h1 class="pb-1 text-pink font-weight-bold">About Parul University’s Study Abroad</h1>
                 <p class="pt-4 text-justify">The Parul University Study Abroad Center provides students with leading opportunities to pursue their studies abroad under the guidance of leading experts.The University’s ties with strategic Universities worldwide across Europe, Australia, USA, Canada, amongst others. We provide the best assistance in language coaching, admissions, and visa processing which allows you to reach your academic dreams. </p>
             </div>
             <div class="col-lg-6">
                 <img src="images/images/Finalised-Study-Abroad-LP/Learning M.Pharm at Parul University.png" class="img-fluid w-100">
             </div>
         </div>

     </div>
 </section>
 <!-- >About Parul University section ends -->



 <!-- Choose The Parul University Experience! section starts -->
 <section class="experience mt-5 py-lg-5 bg-leader-img" id="courses">
     <div class="container-fluid py-5">
         <div class="row">
             <div class="col-md-12 text-center">
                 <h1 class="pb-3 text-pink font-weight-bold">Choose The Parul University Experience!</h1>
             </div>
         </div>

         <div class="row pt-5 justify-content-center">
             <div class="col-md-2 col-6 text-center">
                 <h5 class="text-blue font-weight-bold mt-1 head">2000+</h5>
                 <p class="text-black font-weight-600">Faculties</p>
             </div>
             <div class="col-md-2 col-6 text-center">
                 <h5 class="text-blue font-weight-bold mt-1 head">75+</h5>
                 <p class="text-black font-weight-600">Foreign <br>Partnerships</p>
             </div>
             <div class="col-md-2 col-6 text-center">
                 <h5 class="text-blue font-weight-bold mt-1 head">17000+</h5>
                 <p class="text-black font-weight-600">Placements in<br> 1,600+ Companies</p>
             </div>
             <div class="col-md-2 col-6 text-center">
                 <h5 class="text-blue font-weight-bold mt-1 head">12000+</h5>
                 <p class="text-black font-weight-600">In-Campus<br> Residency</p>
             </div>
             <div class="col-md-2 col-6 text-center">
                 <h5 class="text-blue font-weight-bold mt-1 head">150</h5>
                 <p class="text-black font-weight-600">Acres<br> Campus</p>
             </div>

         </div>

         <div class="row pt-5 mt-3 justify-content-center">
             <div class="col-md-2 col-6 text-center">
                 <h5 class="text-blue font-weight-bold mt-1 head">35,000+</h5>
                 <p class="text-black font-weight-600">Students</p>
             </div>
             <div class="col-md-2 col-6 text-center">
                 <h5 class="text-blue font-weight-bold mt-1 head">2000+</h5>
                 <p class="text-black font-weight-600">International<br>Students</p>
             </div>
             <div class="col-md-2 col-6 text-center">
                 <h5 class="text-blue font-weight-bold mt-1 head">100+</h5>
                 <p class="text-black font-weight-600">National <br>Awards &<br> Ranking</p>
             </div>
             <div class="col-md-2 col-6 text-center">
                 <h5 class="text-blue font-weight-bold mt-1 head">6 Cr</h5>
                 <p class="text-black font-weight-600">Research Grants</p>
             </div>
             <div class="col-md-2 col-6 text-center">
                 <h5 class="text-blue font-weight-bold mt-1 head">10 Cr</h5>
                 <p class="text-black font-weight-600">Entrepreneurship<br> Funding</p>
             </div>

         </div>
     </div>
 </section>
 <!-- eChoose The Parul University Experience!  section ends -->



 <!-- WHY PU Study Abroad Center section starts -->
 <section class="container-fluid common-spacing">
     <div class="container">
         <div class="row">
             <div class="col-md-12">
                 <h1 class="pb-1 text-violet font-weight-bold text-center">WHY PU Study Abroad Center</h1>
             </div>
         </div>
         <div class="row gge-row text-center text-white">
             <div class="col-md-3 gge-col">
                 <div class="violet-bg">
                     <h4 class="mb-0">100% Alignment with Leading Universities</h4>
                 </div>
             </div>
             <div class="col-md-3 gge-col">
                 <div class="pink-bg">
                     <h4 class="mb-0">98% Language Coaching Success Rate</h4>
                 </div>
             </div>
             <div class="col-md-3 gge-col">
                 <div class="violet-bg">
                     <h4 class="mb-0">Effective Career Guidance & Grooming</h4>
                 </div>
             </div>
             <div class="col-md-3 gge-col">
                 <div class="pink-bg">
                     <h4 class="mb-0">Highly Strategic Global Partnerships</h4>
                 </div>
             </div>
         </div>
     </div>
 </section>
 <!-- WHY PU Study Abroad Center section ends -->



 <!-- We Help You Study Abroad section starts -->
 <section class="practicalearning pt-lg-5" id="facilities">
     <div class="container">
         <div class="row">
             <div class="col-md-12 text-center">
                 <h4><strong>Here Is How </strong> </h4>
                 <h1 class="pb-3 text-pink font-weight-bold">We Help You Study Abroad</h1>
             </div>
         </div>
         <div class="row pt-5">
             <div class="practical-learning">
                 <div class="col-md-3 text-center pdd-5">
                     <img src="images/images/Finalised-Study-Abroad-LP/Free Counseling.svg" class="img-fluid pb-3">
                     <p class="text-violet1 font-weight-600">Free Counseling</p>
                     <p class="we-help text-center">Make the best well informed choices through our counseling free of cost</p>
                 </div>
                 <div class="col-md-3 text-center pdd-5">
                     <img src="images/images/Finalised-Study-Abroad-LP/Flight Ticket Booking.svg" class="img-fluid pb-3">
                     <p class="text-violet1 font-weight-600">Flight Ticket Booking</p>
                     <p class="we-help text-center">We ensure that you get the best travel plan through flight bookings</p>
                 </div>
                 <div class="col-md-3 text-center pdd-5">
                     <img src="images/images/Finalised-Study-Abroad-LP/Visa Application.svg" class="img-fluid pb-3">
                     <p class="text-violet1 font-weight-600">Visa Application</p>
                     <p class="we-help text-center">Our experts take you through the preparation for your visa documentations</p>
                 </div>
                 <div class="col-md-3 text-center pdd-5">
                     <img src="images/images/Finalised-Study-Abroad-LP/Admission Processing.svg" class="img-fluid pb-3">
                     <p class="text-violet1 font-weight-600">Admission Processing</p>
                     <p class="we-help text-center">We help you find the best Universities Abroad for your higher studies</p>
                 </div>
                 <div class="col-md-3 text-center pdd-5">
                     <img src="images/images/Finalised-Study-Abroad-LP/University Alignment.svg" class="img-fluid pb-3">
                     <p class="text-violet1 font-weight-600">University Alignment</p>
                     <p class="we-help text-center"> Aligning you with best University which meets academics your needs</p>
                 </div>
                 <div class="col-md-3 text-center pdd-5">
                     <img src="images/images/Finalised-Study-Abroad-LP/Language Coaching.svg" class="img-fluid pb-3">
                     <p class="text-violet1 font-weight-600">Language Coaching</p>
                     <p class="we-help text-center">IELTS, German and French Language Coaching for Your Next Destination</p>
                 </div>
                 <div class="col-md-3 text-center pdd-5">
                     <img src="images/images/Finalised-Study-Abroad-LP/Pre-Departure Orientation.svg" class="img-fluid pb-3">
                     <p class="text-violet1 font-weight-600">Pre-Departure Orientation</p>
                     <p class="we-help text-center">Get prepared to venture to your next global University</p>
                 </div>

             </div>
         </div>
     </div>
 </section>
 <!-- We Help You Study Abroad section ends -->



 <!-- Our Students Who Made It Abroad section starts -->
 <section class="placement my-5 pt-lg-5" id="industry-standards">
    <div class="container">
        <div class="row pb-4">
            <div class="col-md-12 text-center">
                <h1 class="pb-1 text-violet font-weight-bold text-center">Our Students Who Made It Abroad</h1>
            </div>
        </div>
           <div class="row industry-standards" id="industry-standards-data">
               <div class="col-md-3 position-relative">
                   <p class="color-yellow">Hobotiana Elvinah Randrianaivo & Lucian Ratsimamitaka</p>
                   <p class="text-white">
                       University of Paris Saclay, France
                   </p>
                   <img src="images/images/Finalised-Study-Abroad-LP/Icon ionic-md-arrow-dropdown.svg" class="position-absolute img-right">
               </div>
               <div class="col-md-3 bg-image">
               </div>
               <div class="col-md-3 position-relative">
                   <p class="color-yellow">AKSHAR PATEL</p>
                   <p class="text-white">
                       Fachhochschule des Mittelstands (FHM) Germany
                   </p>
                   <img src="images/images/Finalised-Study-Abroad-LP/Icon ionic-md-arrow-dropdown.svg" class="position-absolute img-right">
               </div>
               <div class="col-md-3 bg-image">
               </div>
               <div class="col-md-3 bg-image">
               </div>
               <div class="col-md-3 position-relative">
                   <p class="color-yellow">Hobotiana Elvinah Randrianaivo & Lucian Ratsimamitaka</p>
                   <p class="text-white">
                       University of Paris Saclay, France
                   </p>
                   <img src="images/images/Finalised-Study-Abroad-LP/Icon ionic-md-arrow-dropdown-1.svg" class="position-absolute img-left">
               </div>
               <div class="col-md-3 bg-image">
               </div>
               <div class="col-md-3 position-relative">
                   <p class="color-yellow">AKSHAR PATEL</p>
                   <p class="text-white">
                       Fachhochschule des Mittelstands (FHM) Germany
                   </p>
                   <img src="images/images/Finalised-Study-Abroad-LP/Icon ionic-md-arrow-dropdown-1.svg" class="position-absolute img-left">
               </div>
           </div>
    </div>
</section>
 <!-- Our Students Who Made It Abroad section End -->



 <!-- Providing The Right Language Training For Future Education Start -->
 <section class="my-5 pt-lg-5" id="event">
     <div class="container">
         <div class="row">
             <div class="col-lg-6">
                 <h1 class="pb-1 text-violet font-weight-bold">Providing The Right Language Training For Future Education</h1>
                 <p class="pt-3">The Parul University Study Abroad Center provides students with leading opportunities to pursue their studies abroad under the guidance of leading experts. The University’s ties with strategic Universities worldwide across Europe, Australia, USA, Canada, amongst others. We provide the best assistance in language coaching, admissions, and visa processing which allows you to reach your academic dreams.</p>
             </div>
             <div class="col-lg-6">
                 <h5 class="font-weight-bold mb-0">Why Learn Languages with PU</h5>
                 <ul class="pt-4">
                     <li><span class="ml-3">mock tests<span></li>
                     <li><span class="ml-3">Free 1 week demo class<span></li>
                     <li><span class="ml-3">500+ student enrollment<span></li>
                     <li><span class="ml-3">Trainers with 10+ years of IELTS teaching experience<span></li>
                     <li><span class="ml-3">Options for online and offline classes<span></li>
                     <li><span class="ml-3">Exam oriented coaching<span></li>
                     <li><span class="ml-3">Special weekend batches<span></li>
                     <li><span class="ml-3">Skill focused Learning<span></li>
                     <li><span class="ml-3">Long term membership with a one-time fee payment<span></li>

                 </ul>

             </div>
         </div>
     </div>
 </section>
 <!-- Providing The Right Language Training For Future Education End -->


 <!-- A Glimpse of Study Abroad Possibilities section starts -->
 <section class="mt-5 mb-5 abroad-possibilities" id="campus">
     <div class="container">
         <h2 class="about_header_title text-center pt-4 pb-2 ">A Glimpse of Study Abroad Possibilities</h2>

         <div class="row" style="justify-content: center;">
             <?php
                $country_image_list = array(
                    "images/images/Finalised-Study-Abroad-LP/Rowan University.png",
                    "images/images/Finalised-Study-Abroad-LP/Nottingham Trent University.png",
                    "images/images/Finalised-Study-Abroad-LP/Neoma Business School.png",
                    "images/images/Finalised-Study-Abroad-LP/Caucasus University.png",
                    "images/images/Finalised-Study-Abroad-LP/NIAS & other countries.png"
                );
                $country_list = array("USA", "UK", "France", "Georgia", "Japan");
                $country_university_list = array(
                    "Rowan University <br> University of Detroit Mercy <br> National University <br>Western Illinois University",
                    "Nottingham Trent University <br> Bristol University <br> Canterbury Christ Church University",
                    "Neoma Business School <br> College De Paris <br> Rennes School of Business",
                    "Caucasus University",
                    "NIAS & other countries"
                );
                $m = 0;
                while ($m < 5) { ?>
                 <div class="country-box">

                     <img src="<?php echo $country_image_list[$m]; ?> ">
                     <p><strong class="country-name"><?php echo $country_list[$m]; ?></strong><br>
                         <?php echo $country_university_list[$m]; ?></p>

                     </p>
                 </div>
             <?php
                    $m++;
                }
                ?>
         </div>
     </div>
 </section>
 <!-- A Glimpse of Study Abroad Possibilities section ends -->



 <!-- They Did It, So Can You section starts -->
 <section class="advantage my-5 pt-lg-0" id="recruiters">
     <div class="container">
         <div class="row pt-5">
             <div class="col-lg-4 col-sm-6 d-flex align-items-end">
                 <div class="pb-5">
                     <h1 class="pb-1 text-pink font-weight-bold">They Did It, So Can You</h1>
                     <h6 class="font-weight-bold mb-0">Our Successful Students</h6>
                 </div>
             </div>

             <div class="col-lg-4 col-sm-6">
                 <img src="images/images/Finalised-Study-Abroad-LP/They Did It, So can you Image.png" alt="" class="img-fluid w-75">
                 <div class="adv-sec text-center adv-ph">
                     <h6 class="font-weight-bold mb-0">Babyesh Vithlani</h6>
                     <p>7.5 IELTS Score</p>
                 </div>
             </div>
             <div class="col-lg-4 col-sm-6">
                 <img src="images/images/Finalised-Study-Abroad-LP/They Did It, So can you Image.png" alt="" class="img-fluid w-75">
                 <div class="adv-sec text-center adv-ph">
                     <h6 class="font-weight-bold mb-0">Harshrajsinh Jadeja</h6>
                     <p>7.5 IELTS Score</p>
                 </div>
             </div>
             <div class="col-lg-4  col-sm-6">
                 <img src="images/images/Finalised-Study-Abroad-LP/They Did It, So can you Image.png" alt="" class="img-fluid w-75">
                 <div class="adv-sec text-center adv-ph">
                     <h6 class="font-weight-bold mb-0">Jay Gadhiya</h6>
                     <p>7.5 IELTS Score</p>
                 </div>
             </div>
             <div class="col-lg-4  col-sm-6">
                 <img src="images/images/Finalised-Study-Abroad-LP/They Did It, So can you Image.png" alt="" class="img-fluid w-75">
                 <div class="adv-sec text-center adv-ph">
                     <h6 class="font-weight-bold mb-0">Hishwa Surti</h6>
                     <p>7.5 IELTS Score</p>
                 </div>
             </div>
             <div class="col-lg-4  col-sm-6">
                 <img src="images/images/Finalised-Study-Abroad-LP/They Did It, So can you Image.png" alt="" class="img-fluid w-75">
                 <div class="adv-sec text-center adv-ph">
                     <h6 class="font-weight-bold mb-0">Vraj Pandya</h6>
                     <p>German A1 Score 60/100</p>
                 </div>
             </div>

         </div>

     </div>
 </section>
 <!-- They Did It, So Can You section ends -->



 <!-- Scholarship Opportunities To Support Your Learning Start-->
 <section class="back-img py-5">
     <div class="container">
         <div class="row">
             <div class="col-md-12">
                 <h4 class="text-center">Scholarship Opportunities To Support Your Learning</h4>
                 <p class="text-center">Scholarships are available for students who achieve a band score of 7.5 or higher, with no less than 6 bands in any skill.</p>

             </div>
         </div>
     </div>
 </section>
 <!-- Scholarship Opportunities To Support Your Learning End-->



 <!-- Our Happy Learners section starts -->
 <section class="container-fluid common-spacing testi-sec mb-5" id="testimonials">
     <div class="container">
         <div class="row">
             <div class="col-md-12">
                 <h1 class="pb-1 text-violet font-weight-bold text-center mb-5">Our Happy Learners</h1>
             </div>
         </div>
         <div class="row testi-row">
             <div class="col-md-6">
                 <div class="test-box text-center h-100">
                     <img src="images/images/Finalised-Study-Abroad-LP/Our Students Speak Inverted Commas1-1.svg" alt="Our Students Speak Inverted Commas1" class="img-fluid mb-2">
                     <p class="mb-4">The training and placement cell puts in a lot of hard work to conduct placement drives and prepares students for the selection process. The cell helped me in shaping my professional ethics and mannerism towards a path of employability.</p>
                     <h5 class="mb-0">Uzair Patel</h5>
                     <img src="images/images/Finalised-Study-Abroad-LP/Jayraj Barot-1.png" alt="Our Students Speak_Image" class="img-fluid std-img">
                 </div>
             </div>
             <div class="col-md-6">
                 <div class="test-box text-center h-100">
                     <img src="images/images/Finalised-Study-Abroad-LP/Our Students Speak Inverted Commas1-1.svg" alt="Our Students Speak Inverted Commas1" class="img-fluid">
                     <p class="mb-4">PU provided me an ideal platform to evolve and grow my personality to match the current expectations of the industry and the world of business. The quality teachers and the teaching practices were vital towards making me adapt to the current world trends.</p>
                     <h5 class="mb-0">Shubham Tiwari</h5>
                     <img src="images/images/Finalised-Study-Abroad-LP/Jayraj Barot-1.png" alt="Our Students Speak_Image" class="img-fluid std-img">
                 </div>
             </div>
         </div>
     </div>
 </section>
 <!-- Our Happy Learners section ends -->



 <?php include('footer.php'); ?>